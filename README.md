### Init configs

Go into `configs/` and modify the provider config file with the right options.

### Setup cluster
`source_helpers` contains all helper functions that can be used by the user to
set up the operator cluster.

There are generic helpers like `azure_ips.sh`, `get_qcow2.sh`, `remove-ns.sh`,
and provider-specific ones, called like the provider. Not all providers have helpers.

Every provider has its own source_helper script, and its own config.
Please edit first the config, then source the helper and run the desired function.

### How to install peerpods

1. Optional: if you want to change operator version, user ID (used to name resources) and ssh key location, use env vars `OPERATOR_VERSION` and `SSH_KEY_LOCATION`.<br>
    If you don't have an ssh key, run `ssh-keygen -f ./id_rsa -N ""` and give the path
    to `SSH_KEY_LOCATION`.
2. Optional: if you want to upload your own osc catalog and operator, modify env variable `CATALOG_TAG` in `install_peerpods.sh`
2. Optional: Only for `libvirt` and `aro` providers: Modify env variables at the respective configs in `configs/*`
3. Run `install_peerpods.sh`. When prompted, chose which provider to use. Alternatively, provide `-p <provider>`. `-c` provides the possibility to compile your own operator. `-o` to install the operator too.

### How to delete peerpods
1. Run `delete_peerpods.sh`. Uses the same `common_yaml.sh` variables. Also here it is possible to use `-p <provider>`.

