#! /bin/bash
set -e

OPERATOR_VERSION=${OPERATOR_VERSION:-"1.7.0"}
SSH_KEY_LOCATION=${SSH_KEY_LOCATION:-"/home/eesposit/openshift/id_rsa"}

### Only used for compilation ###
# Provide your repo base and tag for compiling and uploading the operator.
# The repo will be created the first time this process is done.
# It is however necessary to manually go to quay.io and set the repo as public.
# It is also mandatory to have logged in to quay.io via 'docker login quay.io'
export IMAGE_TAG_BASE=quay.io/eesposit/osc-operator
export IMG=${IMAGE_TAG_BASE}:tech-prev
# Where the code resides locally
OPERATOR_LOCATION=~/sandboxed-containers-operator

# Compiled catalog name
CATALOG_NAME=my-operator-catalog

## Repo base and tag for the catalog. Again this repo is generated the first time,
## and it needs to be made public, together with the bundle repo (check quay.io)
CATALOG_TAG=${IMAGE_TAG_BASE}-catalog:v${IMAGE_TAG_BASE}
## special catalog tag prepared by pradipta. Use only when not compiling.
# CATALOG_TAG=quay.io/bpradipt/openshift-sandboxed-containers-operator-catalog:v${IMAGE_TAG_BASE}
## Get the latest OSC. Get the latest tag from
## skopeo list-tags docker://quay.io/openshift_sandboxed_containers/openshift-sandboxed-containers-operator-catalog
# CATALOG_TAG=quay.io/openshift_sandboxed_containers/openshift-sandboxed-containers-operator-catalog:${IMAGE_TAG_BASE}

# In case we need to use a custom CAA, define UPSTREAM_CAA
# UPSTREAM_CAA=quay.io/eesposit/cloud-api-adaptor:latest
# UPSTREAM_CAA=quay.io/confidential-containers/cloud-api-adaptor:latest

# Used internally, do not modify
SUBSCRIPTION_SOURCE=redhat-operators

source common_yaml.sh
source_all_providers

usage="$(basename $0) [-h] [-c] [-p <aws/aro/azure/libvirt>]\n
where:
    -h  show this help text
    -c  compile (default NO)
    -o  install the operator (default NO)
    -p  provider to use. If not given, it will ask in stdin
    -e  if you do *not* want to run hello-openshift."

while getopts 'hcop:e' option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    c) COMPILE="yes"
       SUBSCRIPTION_SOURCE=$CATALOG_NAME
       ;;
    o) OPERATOR="yes"
       ;;
    p) cld=${OPTARG}
       ;;
    e) DISABLE_HELLO="yes"
       ;;
  esac
done

take_provider_from_input

provider=$cld
provider_update="${cld}_update"
provider_post="${cld}_post"
source $(dirname $0)/configs/$provider.conf

check_providers

if [ ! -z $COMPILE ]; then
    echo "#### Compiling..."
    compile_operator

    echo "#### Creating CatalogSource..."
    oc_command apply -f-<<EOF
${CatalogSource}
EOF

    # install the operator
    OPERATOR="yes"
fi

if [ ! -z $OPERATOR ]; then
    # echo "#### Creating ImageContentSourcePolicy..."
    # oc_command apply -f-<<EOF
# ${ImageContentSourcePolicy}
# EOF

    echo "#### Creating Namespace..."
    oc_command create -f-<<EOF
${Namespace}
EOF

    echo "#### Creating OperatorGroup..."
    oc_command create -f-<<EOF
${OperatorGroup}
EOF

    echo "#### Creating Subscription..."
    oc_command create -f-<<EOF
${Subscription}
EOF

fi


## Wait it is installed
echo "#### Waiting for controller-manager..."
cmd="wait --for=condition=Available=true deployment.apps/controller-manager --timeout=3m -n $OP_NAME"
wait_for_oc_command $cmd

# Fix wrong upstream variable, only necessary when deploy your own catalog
# echo "#### Fixing wrong env variable..."
# oc_command set env deployment.apps/controller-manager SANDBOXED_CONTAINERS_EXTENSION=sandboxed-containers -n $OP_NAME

# provider-specific code
$provider

sleep 10

# Create kataconfig
echo "#### Creating KataConfig..."
oc_command apply -f-<<EOF
${KataConfig}
EOF

echo "#### Waiting for KataConfig to be created..."
cmd="wait --for=condition=Updating=false machineconfigpool/kata-oc --timeout=-1s"
wait_for_oc_command $cmd

echo "#### Waiting for Job to be finished..."
cmd="wait --for=condition=complete job/osc-podvm-image-creation -n $OP_NAME --timeout=-1s"
wait_for_oc_command $cmd

echo "#### Waiting for Peerpods Daemon to be started..."
cmd="rollout status daemonset peerpodconfig-ctrl-caa-daemon -n $OP_NAME --timeout=60s"
wait_for_oc_command $cmd

echo "Peer Pods has been installed on your cluster"

if [ ! -z $UPSTREAM_CAA ]; then
    oc_command set image ds/peerpodconfig-ctrl-caa-daemon -n $OP_NAME cc-runtime-install-pod=$UPSTREAM_CAA
    echo "REINSTALLED CAA"
    oc_command rollout status daemonset peerpodconfig-ctrl-caa-daemon -n $OP_NAME --timeout=60s

    $provider_update 2> /dev/null || echo "No provider update for ${provider}"
    sleep 10
fi

$provider_post 2> /dev/null

# do not apply hello-openshift, if it is specified
if [ ! -z $DISABLE_HELLO ]; then
    exit
fi

echo "#### Creating Hello Openshift..."
oc_command apply -f-<<EOF
${HelloOpenshift}
EOF

sleep 10

oc_command expose service hello-openshift-service -l app=hello-openshift
echo "Waiting that hello world pod is running... (might take a while)"
cmd="wait --for=condition=Ready pod/hello-openshift --timeout=-1s"
wait_for_oc_command $cmd
oc_command get pod/hello-openshift

echo "Testing the hello world example..."
APP_URL=$(oc_command get routes/hello-openshift-service -o jsonpath='{.spec.host}')
curl $APP_URL