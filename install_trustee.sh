#! /bin/bash
set -e

source common_yaml.sh

# echo "#### Creating CatalogSource..."
# oc_command create -f-<<EOF
# ${TrusteeCatalogSource}
# EOF

echo "#### Creating Namespace..."
oc_command apply -f-<<EOF
${TrusteeNamespace}
EOF

echo "#### Creating OperatorGroup..."
oc_command apply -f-<<EOF
${TrusteeOperatorGroup}
EOF

echo "#### Creating Subscription..."
oc_command apply -f-<<EOF
${TrusteeSubscription}
EOF

echo "#### Waiting for trustee..."
cmd="wait --for=condition=Available=true deployment.apps/trustee-operator-controller-manager --timeout=3m -n $TR_NAME"
wait_for_oc_command $cmd

echo "#### Creating a route..."
oc_command create route edge --service=kbs-service --port kbs-port -n $TR_NAME
TRUSTEE_HOST=$(oc_command get route -n $TR_NAME kbs-service -o jsonpath={.spec.host})
echo $TRUSTEE_HOST

echo "#### Creating the secret..."
openssl genpkey -algorithm ed25519 > privateKey
openssl pkey -in privateKey -pubout -out publicKey
oc_command create secret generic kbs-auth-public-key --from-file=publicKey -n $TR_NAME
# oc_command get secret -n $TR_NAME

echo "#### Creating CM..."
oc_command apply -f-<<EOF
${TrusteeCM}
EOF

echo "#### Creating Reference Values..."
oc_command apply -f-<<EOF
${TrusteeRV}
EOF

oc_command create secret generic kbsres1 --from-literal key1=Trustee_Secret_Retrieved -n $TR_NAME # --from-file key.bin=key.bin

echo "#### Creating Resource Policies..."
oc_command apply -f-<<EOF
${TrusteeRP}
EOF

echo "#### Creating KBS Config..."
oc_command apply -f-<<EOF
${KBSConfig}
EOF

POD_NAME=$(oc get pods -l app=kbs -o jsonpath='{.items[0].metadata.name}' -n $TR_NAME)
oc logs -n $TR_NAME $POD_NAME

echo "Trustee has been installed on your cluster"