#! /bin/bash

aws_run_image_job () {
    curl -L https://red.ht/pp-azure-image-job | sed 's#registry.redhat.io/openshift-sandboxed-containers/osc-podvm-payload:1.4.0#quay.io/snir/podvm-binaries:1.4.0#' | oc_command apply -f -

    echo "Waiting that image creation job is complete..."
    oc_command wait --for=condition=complete job.batch/azure-image-creation --timeout=7m -n $OP_NAME
}

aws_get_secret_credentials () {
    INSTANCE_ID=$(oc_command get nodes -l 'node-role.kubernetes.io/worker' -o jsonpath='{.items[0].spec.providerID}' | sed 's#[^ ]*/##g')

    AWS_REGION=$(oc_command get infrastructure/cluster -o jsonpath='{.status.platformStatus.aws.region}')

    AWS_SUBNET_ID=$(aws ec2 describe-instances --instance-ids ${INSTANCE_ID} --query 'Reservations[*].Instances[*].SubnetId' --region $AWS_REGION --output text)

    AWS_VPC_ID=$(aws ec2 describe-instances --instance-ids ${INSTANCE_ID} --query 'Reservations[*].Instances[*].VpcId' --region $AWS_REGION --output text)

    AWS_SG_IDS=$(aws ec2 describe-instances --instance-ids ${INSTANCE_ID} --query 'Reservations[*].Instances[*].SecurityGroups[*].GroupId' --region $AWS_REGION --output text)

    echo "AWS_REGION: \"$AWS_REGION\""
    echo "AWS_SUBNET_ID: \"$AWS_SUBNET_ID\""
    echo "AWS_VPC_ID: \"$AWS_VPC_ID\""
    echo "AWS_SG_IDS: \"$AWS_SG_IDS\""
}

aws_open_ports () {
    AWS_SG_ID=$(aws ec2 describe-instances --instance-ids ${INSTANCE_ID} --query 'Reservations[*].Instances[*].SecurityGroups[*].GroupId' --output text --region $AWS_REGION)

    # This is required for peer-pods shim to kata-agent communication
    aws ec2 authorize-security-group-ingress --group-id $AWS_SG_ID --protocol tcp --port 15150 --source-group $AWS_SG_ID --region $AWS_REGION

    # This is required for peer-pods tunnel setup
    aws ec2 authorize-security-group-ingress --group-id $AWS_SG_ID --protocol tcp --port 9000 --source-group $AWS_SG_ID --region $AWS_REGION
}

aws () {
    echo "Make sure AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY are set!"
    aws_get_secret_credentials

    cat <<EOF | oc_command apply -f -
apiVersion: v1
kind: Secret
metadata:
  name: ${PP_SECRET}
  namespace: ${OP_NAME}
type: Opaque
stringData:
  AWS_ACCESS_KEY_ID: "${AWS_ACCESS_KEY_ID}"   #set
  AWS_SECRET_ACCESS_KEY: "${AWS_SECRET_ACCESS_KEY}" #set
  AWS_REGION: "${AWS_REGION}"
  AWS_SUBNET_ID: "${AWS_SUBNET_ID}"
  AWS_VPC_ID: "${AWS_VPC_ID}"
  AWS_SG_IDS: "${AWS_SG_IDS}"
EOF

aws_run_image_job

PODVM_AMI_ID=$(aws ec2 describe-images --query "Images[*].[ImageId]" --filters "Name=name,Values=peer-pod-ami" --region ${AWS_REGION} --output text)

    cat <<EOF | oc_command apply -f -
apiVersion: v1
kind: ConfigMap
metadata:
  name: ${CM}
  namespace: ${OP_NAME}
data:
  CLOUD_PROVIDER: "aws"
  VXLAN_PORT: "9000"
  PODVM_INSTANCE_TYPE: "t3.small"
  PROXY_TIMEOUT: "5m"
  PODVM_AMI_ID: ${PODVM_AMI_ID}
EOF

    aws_open_ports
}

aws_del () {
  :
}
