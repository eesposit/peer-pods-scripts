#! /bin/bash

source providers/microsoft/common.sh
source $(dirname $0)/configs/aro.conf # needed AZURE_INSTANCE_SIZE, ARO_RESOURCE_GROUP

# This key is created because kcli needed it to create the cluster.
# The k8s secret contains the private key and caa pod uses it to connect to azure
# which has the public one in it's /root/.ssh/authorized_keys
ARO_KEY=$SSH_KEY_LOCATION

# query existing service principal to get keys from it
# Requires: az login and oc login already done!
aro_get_secret_credentials () {
    azure_get_subscription
    azure_get_resource_region

    AZURE_CLIENT_ID=$(oc_command -n kube-system get secrets azure-credentials -o json | jq -r '.data."azure_client_id" | @base64d')
    AZURE_CLIENT_SECRET=$(oc_command -n kube-system get secrets azure-credentials -o json | jq -r '.data."azure_client_secret" | @base64d')
    AZURE_TENANT_ID=$(oc_command -n kube-system get secrets azure-credentials -o json | jq -r '.data."azure_tenant_id" | @base64d')

    azure_print_credentials
}

aro () {
    aro_get_secret_credentials
    azure_apply_pp_secret
    azure_apply_configmap $ARO_RESOURCE_GROUP
    azure_add_secret $ARO_KEY
}

aro_del () {
    azure_rm_secret
    azure_del_coco_image_job
}

# aro_post () {
#     azure_post_kataconfig
# }