#! /bin/bash

azure_get_subscription () {
    AZURE_SUBSCRIPTION_ID=$(az account list --query "[?isDefault].id" -o tsv)
    # AZURE_SUBSCRIPTION_ID=$(oc_command -n kube-system get secrets azure-credentials -o json | jq -r '.data."azure_subscription_id" | @base64d')
}

azure_get_resource_region () {
    # Cluster resource group.
    # In ARO, this is created internally, while ARO_RESOURCE_GROUP is the user created.
    # In AZURE, this is created by the user.
    AZURE_RESOURCE_GROUP=$(oc_command get infrastructure/cluster -o jsonpath='{.status.platformStatus.azure.resourceGroupName}')
    AZURE_REGION=$(az group show --resource-group $AZURE_RESOURCE_GROUP --query "{Location:location}" --output tsv)
    # AZURE_RESOURCE_GROUP=$(oc_command -n kube-system get secrets azure-credentials -o json | jq -r '.data."azure_resourcegroup" | @base64d')
    # AZURE_REGION=$(oc_command -n kube-system get secrets azure-credentials -o json | jq -r '.data."azure_region" | @base64d')
}

azure_print_credentials () {
    echo "AZURE_CLIENT_ID: \"$AZURE_CLIENT_ID\""
    echo "AZURE_CLIENT_SECRET: \"$AZURE_CLIENT_SECRET\""
    echo "AZURE_SUBSCRIPTION_ID: \"$AZURE_SUBSCRIPTION_ID\""
    echo "AZURE_RESOURCE_GROUP: \"$AZURE_RESOURCE_GROUP\""
    echo "AZURE_REGION: \"$AZURE_REGION\""
}

azure_apply_pp_secret () {
    # USER_RG=$1
    AzurePPSecret="\
apiVersion: v1
kind: Secret
metadata:
  name: ${PP_SECRET}
  namespace: ${OP_NAME}
type: Opaque
stringData:
  AZURE_CLIENT_SECRET: ${AZURE_CLIENT_SECRET}
  AZURE_CLIENT_ID: ${AZURE_CLIENT_ID}
  AZURE_TENANT_ID: ${AZURE_TENANT_ID}
  AZURE_SUBSCRIPTION_ID: ${AZURE_SUBSCRIPTION_ID}
  AZURE_REGION: ${AZURE_REGION}
  AZURE_RESOURCE_GROUP: ${AZURE_RESOURCE_GROUP}"

#     if [ -z $USER_RG ]; then
#         AzurePPSecret="${AzurePPSecret}
#   AZURE_USER_RESOURCE_GROUP: ${USER_RG}"
#     fi

    oc_command apply -f-<<EOF
${AzurePPSecret}
EOF
}

azure_run_coco_image_job () {
    if [[ $AZURE_INSTANCE_SIZE == *DC* ]]; then
        oc_command apply -f https://raw.githubusercontent.com/openshift/sandboxed-containers-operator/coco-dev-preview/hack/azure-CVM-image-create-job.yaml

        echo "Waiting that CoCo image creation job is complete..."
        oc_command wait --for=condition=complete job.batch/azure-confidential-image-creation --timeout=20m -n $OP_NAME

        AZURE_IMAGE_ID=$(az image list --resource-group $AZURE_RESOURCE_GROUP --query "[].{Id: id} | [? contains(Id, 'cvm-rhel-image')]" --output tsv)

        oc_command get cm/peer-pods-cm -n openshift-sandboxed-containers-operator -o json | jq --arg IMG "$AZURE_IMAGE_ID" '.data.AZURE_IMAGE_ID = $IMG' | oc_command replace -f -
    fi
}

azure_del_coco_image_job () {
    if [[ $AZURE_INSTANCE_SIZE == *DC* ]]; then
        oc_command apply -f https://raw.githubusercontent.com/openshift/sandboxed-containers-operator/coco-dev-preview/hack/azure-CVM-image-delete-job.yaml

        echo "Waiting that CoCo image deletion job is complete..."
        oc_command wait --for=condition=complete job.batch/azure-confidential-image-deletion --timeout=20m -n $OP_NAME
    fi
}

azure_apply_configmap () {
    VNET_RG=$1
    AZURE_INSTANCE_SIZE=${AZURE_INSTANCE_SIZE:-"Standard_D8as_v5"}

    AZURE_VNET_NAME=$(az network vnet list --resource-group $VNET_RG --query "[].{Name:name}" --output tsv)
    AZURE_SUBNET_ID=$(az network vnet subnet list --resource-group $VNET_RG --vnet-name $AZURE_VNET_NAME --query "[].{Id:id} | [? contains(Id, 'worker')]" --output tsv)

    AZURE_NSG_ID=$(az network nsg list --resource-group $AZURE_RESOURCE_GROUP --query "[].{Id:id}" --output tsv)

    AzureConfigMap="\
apiVersion: v1
kind: ConfigMap
metadata:
  name: ${CM}
  namespace: ${OP_NAME}
data:
  CLOUD_PROVIDER: azure
  VXLAN_PORT: '9000'
  AZURE_INSTANCE_SIZE: ${AZURE_INSTANCE_SIZE}
  PROXY_TIMEOUT: 5m
  AZURE_SUBNET_ID: ${AZURE_SUBNET_ID}
  AZURE_NSG_ID: ${AZURE_NSG_ID}
  AZURE_REGION: ${AZURE_REGION}
  AZURE_RESOURCE_GROUP: ${AZURE_RESOURCE_GROUP}
  AZURE_IMAGE_ID: ''"

    # Check for CoCo
    if [[ $AZURE_INSTANCE_SIZE == *DC* ]]; then
        AzureConfigMap="${AzureConfigMap}
  DISABLECVM: 'false'"

        oc_command apply -f-<<EOF
${FeatureGate}
EOF
    else
        AzureConfigMap="${AzureConfigMap}
  DISABLECVM: 'true'"
    fi

    oc_command apply -f-<<EOF
${AzureConfigMap}
EOF

    # if [[ $AZURE_INSTANCE_SIZE == *DC* ]]; then
    #     # Needs AZURE_RESOURCE_GROUP and AZURE_REGION to be set in cm/secret
    #     azure_run_coco_image_job
    # fi
}

azure_post_kataconfig () {
    if [[ ! $AZURE_INSTANCE_SIZE == *DC* ]]; then
        return
    fi

    oc_command apply -f https://raw.githubusercontent.com/openshift/sandboxed-containers-operator/coco-dev-preview/hack/ds.yaml

    oc_command apply -f https://raw.githubusercontent.com/openshift/sandboxed-containers-operator/coco-dev-preview/hack/mc-coco.yaml

    sleep 10

    echo "#### Waiting for mcp kata-oc..."
    oc_command wait --for=condition=Updated=true mcp kata-oc_command --timeout=-1s

    oc_command set image ds/peerpodconfig-ctrl-caa-daemon -n $OP_NAME cc-runtime-install-pod=quay.io/openshift_sandboxed_containers/cloud-api-adaptor:coco-dev-preview

    oc_command set env ds/peerpodconfig-ctrl-caa-daemon -n $OP_NAME REBOOT="$(date)"
}

azure_rm_configmap () {
    oc_command delete configmaps/$CM -n $OP_NAME
}

azure_add_secret () {
    if [ ! -f $1 ]; then
        ssh-keygen -f $1 -N ""
    fi
    oc_command create secret generic $SSH_SECRET -n $OP_NAME --from-file=id_rsa.pub=$1.pub --from-file=id_rsa=$1
}

azure_rm_secret () {
    oc_command delete secrets/$SSH_SECRET -n $OP_NAME
}