#! /bin/bash

source providers/microsoft/common.sh
source $(dirname $0)/configs/azure.conf # needed AZURE_INSTANCE_SIZE

# This key is created because kcli needed it to create the cluster.
# The k8s secret contains the private key and caa pod uses it to connect to azure
# which has the public one in it's /root/.ssh/authorized_keys
AZURE_KEY=$SSH_KEY_LOCATION

# create service principal and get the keys from it
# Requires: az login and oc login already done!
azure_get_secret_credentials () {
    azure_get_subscription
    azure_get_resource_region

    AZURE_CLIENT_ID=$(search_sp AZURE_CLIENT_ID)
    AZURE_CLIENT_SECRET=$(search_sp AZURE_CLIENT_SECRET)
    AZURE_TENANT_ID=$(search_sp AZURE_TENANT_ID)

    azure_print_credentials
}

azure () {
    azure_get_secret_credentials
    azure_apply_pp_secret
    azure_apply_configmap $AZURE_RESOURCE_GROUP
    azure_add_secret $AZURE_KEY
}

azure_del () {
    azure_rm_secret
    azure_del_coco_image_job
}

# azure_post () {
#     azure_post_kataconfig
# }