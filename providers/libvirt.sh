#! /bin/bash

echo $0
source $(dirname $0)/configs/libvirt.conf

LibvirtSecret="\
apiVersion: v1
kind: Secret
metadata:
  name: ${PP_SECRET}
  namespace: ${OP_NAME}
type: Opaque
stringData:
  CLOUD_PROVIDER: \"libvirt\"
  VXLAN_PORT: \"${LIBVIRT_VXLAN_PORT}\"
  LIBVIRT_URI: \"qemu+ssh://root@${LIBVIRT_NET_IP}.1/system?no_verify=1\"
  LIBVIRT_NET: \"${LIBVIRT_NET_NAME}\"
  LIBVIRT_POOL: \"${LIBVIRT_POOL_NAME}\""

LibvirtConfigMap="\
apiVersion: v1
kind: ConfigMap
metadata:
  name: ${CM}
  namespace: ${OP_NAME}
data:
  CLOUD_PROVIDER: \"libvirt\"
  PROXY_TIMEOUT: \"30m\""

add_libvirt_secret () {
    kubectl create secret generic $SSH_SECRET --from-file=id_rsa=${LIBVIRT_KEY} -n $OP_NAME
}

rm_libvirt_secret () {
    oc_command delete secrets/$SSH_SECRET -n $OP_NAME
}

libvirt () {
    oc_command apply -f-<<EOF
${LibvirtSecret}
EOF

    oc_command apply -f-<<EOF
${LibvirtConfigMap}
EOF

    add_libvirt_secret
}

libvirt_del () {
    rm_libvirt_secret
}

libvirt_update () {
    LibvirtSecret="${LibvirtSecret}
  LIBVIRT_VOL_NAME: \"${LIBVIRT_VOL_NAME}\""

    oc_command apply -f-<<EOF
${LibvirtSecret}
EOF
}