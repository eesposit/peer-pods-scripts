#! /bin/bash

source common_yaml.sh
source_all_providers

usage="$(basename $0) [-h] [-p <aws/aro/azure/libvirt>]\n
where:
    -h  show this help text
    -p  provider to use. If not given, it will ask in stdin."

while getopts 'hcp:' option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    p) cld=${OPTARG}
       ;;
  esac
done

take_provider_from_input

provider="${cld}_del"

check_providers

echo "#### Deleting Hello Openshift..."
oc_command delete all -l app=$HO_NAME

echo "#### Deleting KataConfig..."
echo "Hack: run \"oc edit kataconfigs/$KC_NAME\" in another window and remove \"finalizers:\" and the line below."
oc_command delete kataconfigs/$KC_NAME

echo "#### Deleting ConfigMap..."
oc_command delete configmaps/$CM -n $OP_NAME

echo "#### Deleting PeerPodsSecret..."
oc_command delete secrets/$PP_SECRET -n $OP_NAME

# provider-specific code
$provider

echo "#### Deleting Subscription..."
oc_command delete subscriptions/$S_NAME -n $OP_NAME

echo "#### Deleting OperatorGroup..."
oc_command delete operatorgroups/$OP_NAME -n $OP_NAME

echo "#### Deleting Namespace..."
oc_command delete namespaces/$OP_NAME

# echo "#### Deleting CatalogSource..."
# oc_command delete catalogsources/$CATALOG_NAME -n openshift-marketplace

# echo "#### Deleting ImageContentSourcePolicy..."
# oc_command delete imagecontentsourcepolicies/$ICSP_NAME
