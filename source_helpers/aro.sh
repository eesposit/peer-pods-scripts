#! /bin/bash

source $(dirname $0)/../configs/aro.conf

copy_into_file() {
    file=$1
    var_name=$2
    content="${@:3}"

    echo "$var_name ${content}"
    echo "$var_name=${content}" >> $file
}

# oc username
AZ_USERNAME="kubeadmin"

if [ ! -f "$RH_PULL_SECRET" ]; then
    echo "Error: $RH_PULL_SECRET does not exist."
    return
fi

install_azure_cli () {
    sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
    sudo dnf install -y https://packages.microsoft.com/config/rhel/9.0/packages-microsoft-prod.rpm
    sudo dnf install -y azure-cli
}

print_cluster_info () {
    webURL=$(az aro show \
        --name $ARO_CLUSTER_NAME \
        --resource-group $ARO_RESOURCE_GROUP \
        --query "consoleProfile.url" -o tsv)
    echo "Web url: $webURL"

    AZ_PW=`az aro list-credentials --name $ARO_CLUSTER_NAME --resource-group $ARO_RESOURCE_GROUP | jq -r '.kubeadminPassword'`
    echo "password: $AZ_PW"

    apiServer=$(az aro show -g $ARO_RESOURCE_GROUP -n $ARO_CLUSTER_NAME --query apiserverProfile.url -o tsv)
    echo "login with \"oc login $apiServer -u $AZ_USERNAME -p $AZ_PW\""
}

oc_login_aro () {
    AZ_PW=`az aro list-credentials --name $ARO_CLUSTER_NAME --resource-group $ARO_RESOURCE_GROUP | jq -r '.kubeadminPassword'`

    apiServer=$(az aro show -g $ARO_RESOURCE_GROUP -n $ARO_CLUSTER_NAME --query apiserverProfile.url -o tsv)
    oc login $apiServer -u $AZ_USERNAME -p $AZ_PW
}

install_aro () {
    AZURE_SUBSCRIPTION_ID=$(az account list --query "[?isDefault].id" -o tsv)

    mkdir -p $ARO_FOLDER
    touch $ARO_CLUSTER_INFO_FILE
    copy_into_file $ARO_CLUSTER_INFO_FILE AZURE_SUBSCRIPTION_ID $AZURE_SUBSCRIPTION_ID


    az account set --subscription $AZURE_SUBSCRIPTION_ID
    az provider register -n Microsoft.RedHatOpenShift --wait
    az provider register -n Microsoft.Compute --wait
    az provider register -n Microsoft.Storage --wait
    az provider register -n Microsoft.Authorization --wait

    az group create \
    --name $ARO_RESOURCE_GROUP \
    --location $ARO_LOCATION
    copy_into_file $ARO_CLUSTER_INFO_FILE ARO_RESOURCE_GROUP $ARO_RESOURCE_GROUP


    echo "ARO VNET NAME: ${ARO_VNET_NAME}"

    az network vnet create \
    --resource-group $ARO_RESOURCE_GROUP \
    --name $ARO_VNET_NAME \
    --address-prefixes 10.0.0.0/22
    copy_into_file $ARO_CLUSTER_INFO_FILE ARO_VNET_NAME $ARO_VNET_NAME


    echo "ARO VNET MASTER SUBNET NAME: ${ARO_MASTER_SUBNET_NAME}"

    az network vnet subnet create \
    --resource-group $ARO_RESOURCE_GROUP \
    --vnet-name $ARO_VNET_NAME \
    --name $ARO_MASTER_SUBNET_NAME \
    --address-prefixes 10.0.0.0/23
    copy_into_file $ARO_CLUSTER_INFO_FILE ARO_MASTER_SUBNET_NAME $ARO_MASTER_SUBNET_NAME


    echo "ARO VNET WORKER SUBNET NAME: ${ARO_WORKER_SUBNET_NAME}"

    az network vnet subnet create \
    --resource-group $ARO_RESOURCE_GROUP \
    --vnet-name $ARO_VNET_NAME \
    --name $ARO_WORKER_SUBNET_NAME \
    --address-prefixes 10.0.2.0/23
    copy_into_file $ARO_CLUSTER_INFO_FILE ARO_WORKER_SUBNET_NAME $ARO_WORKER_SUBNET_NAME

}

uninstall_aro () {
    echo "After this, you need to run install_aro"
    az group delete --resource-group $ARO_RESOURCE_GROUP --yes
    rm -rf $ARO_FOLDER
}

create_aro_cluster () {
    if ! test -f "$ARO_CLUSTER_INFO_FILE"; then
        echo "$ARO_CLUSTER_INFO_FILE does not exists. Call install_aro_cluster."
    fi

    AZURE_SUBSCRIPTION_ID=$(az account list --query "[?isDefault].id" -o tsv)
    az account set --subscription $AZURE_SUBSCRIPTION_ID

    echo "Creating..."
    az aro create \
    --resource-group $ARO_RESOURCE_GROUP \
    --name $ARO_CLUSTER_NAME \
    --vnet $ARO_VNET_NAME \
    --master-subnet $ARO_MASTER_SUBNET_NAME \
    --worker-subnet $ARO_WORKER_SUBNET_NAME \
    --version $ARO_CLUSTER_VERSION \
    --pull-secret $RH_PULL_SECRET
    copy_into_file $ARO_CLUSTER_INFO_FILE ARO_CLUSTER_NAME $ARO_CLUSTER_NAME

    echo "Credentials to connect to ARO cluster:"
    az aro list-credentials \
    --name $ARO_CLUSTER_NAME \
    --resource-group $ARO_RESOURCE_GROUP

    echo "Web URL to connect to ARO cluster:"
    az aro show \
        --name $ARO_CLUSTER_NAME \
        --resource-group $ARO_RESOURCE_GROUP \
        --query "consoleProfile.url" -o tsv

    oc_login_aro

    if [ -n "$ARO_NEW_CLUSTER_VERSION" ]; then
        echo "Upgrading cluster to $ARO_NEW_CLUSTER_VERSION"
        oc adm upgrade --allow-explicit-upgrade --force=true --to-image=quay.io/openshift-release-dev/ocp-release:$ARO_NEW_CLUSTER_VERSION-x86_64

        oc wait --for=condition=Progressing=false clusterversion/version --timeout=-1s
        copy_into_file $ARO_CLUSTER_INFO_FILE ARO_NEW_CLUSTER_VERSION $ARO_NEW_CLUSTER_VERSION
    fi

    echo "Cluster creation done!"
}

delete_aro_cluster () {
    echo "Deleting..."
    az aro delete --resource-group $ARO_RESOURCE_GROUP --name $ARO_CLUSTER_NAME --yes
}