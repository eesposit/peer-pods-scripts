#! /bin/bash

source $(dirname $0)/../configs/azure.conf

copy_into_file() {
    file=$1
    var_name=$2
    content="${@:3}"

    echo "$var_name=${content}"
    echo "$var_name=${content}" >> $file
}

create_sp() {
    # DO IT ONLY ONCE!!
    if test -f "$AZURE_SP_INFO"; then
        echo "$AZURE_SP_INFO exists already. Delete this SP first with delete_sp."
    fi

    rm -f ~/.azure/osServicePrincipal.json

    AZURE_SUBSCRIPTION_ID=$(az account list --query "[?isDefault].id" -o tsv)
    copy_into_file $AZURE_SP_INFO AZURE_SUBSCRIPTION_ID $AZURE_SUBSCRIPTION_ID

    az ad sp create-for-rbac --role Contributor --scopes /subscriptions/$AZURE_SUBSCRIPTION_ID --query "{ client_id: appId, client_secret: password, tenant_id: tenant }" | tee tmpfile

    AZURE_CLIENT_ID=$(cat tmpfile |  jq -r .client_id)
    copy_into_file $AZURE_SP_INFO AZURE_CLIENT_ID $AZURE_CLIENT_ID

    AZURE_CLIENT_SECRET=$(cat tmpfile |  jq -r .client_secret)
    copy_into_file $AZURE_SP_INFO AZURE_CLIENT_SECRET $AZURE_CLIENT_SECRET

    AZURE_TENANT_ID=$(cat tmpfile |  jq -r .tenant_id)
    copy_into_file $AZURE_SP_INFO AZURE_TENANT_ID $AZURE_TENANT_ID

    az role assignment create --role "User Access Administrator" \
        --assignee-object-id $(az ad sp show --id $AZURE_CLIENT_ID --query id -o tsv)

    echo "" >> $AZURE_SP_INFO
    rm tmpfile
}

delete_sp() {
    AZURE_CLIENT_ID=$(search_sp AZURE_CLIENT_ID)
    az ad sp delete --id $AZURE_CLIENT_ID
    rm -f $AZURE_SP_INFO
}

install_azure() {
    mkdir -p $AZURE_FOLDER
    touch $AZURE_CLUSTER_INFO_FILE

    AZURE_SUBSCRIPTION_ID=$(az account list --query "[?isDefault].id" -o tsv)
    copy_into_file $AZURE_CLUSTER_INFO_FILE AZURE_SUBSCRIPTION_ID $AZURE_SUBSCRIPTION_ID

    az account set --subscription $AZURE_SUBSCRIPTION_ID

    az group create \
        --name $AZURE_RESOURCE_GROUP \
        --location $AZURE_LOCATION
    copy_into_file $AZURE_CLUSTER_INFO_FILE AZURE_RESOURCE_GROUP $AZURE_RESOURCE_GROUP

    az network dns zone create -g $AZURE_RESOURCE_GROUP -n $DNS_ZONE
    copy_into_file $AZURE_CLUSTER_INFO_FILE DNS_ZONE $DNS_ZONE
}

uninstall_azure() {
    echo "After this, you need to run install_azure"
    az group delete --resource-group $AZURE_RESOURCE_GROUP --yes
    rm -rf $AZURE_FOLDER
}

create_azure_cluster() {

    echo "installing azure in ${AZURE_FOLDER}"
    echo "If you see \"no such host\" errors, edit and run azure_ips.sh and then re-run openshift-install command"

    echo "openshift-install create cluster --dir $AZURE_FOLDER --log-level=info"
    openshift-install --dir=$AZURE_FOLDER create install-config
    echo "Modify $AZURE_FOLDER/install-config.yaml"
    echo -n "Press enter when ready: "
    read -n 1
    dt=$(date '+%Y-%m-%d-%H:%M:%S')

    cp $AZURE_FOLDER/install-config.yaml $AZURE_FOLDER/install-config-$dt.yaml
    openshift-install create cluster --dir $AZURE_FOLDER --log-level=info
}

create_azure_storage() {
    # https://learn.microsoft.com/en-us/azure/storage/blobs/storage-quickstart-blobs-cli
    AZURE_SUBSCRIPTION_ID=$(az account list --query "[?isDefault].id" -o tsv)

    az storage account create \
    --name $AZURE_STORAGE_NAME \
    --resource-group $AZURE_RESOURCE_GROUP \
    --location $AZURE_LOCATION \
    --sku Standard_ZRS \
    --encryption-services blob

    az ad signed-in-user show --query id -o tsv | az role assignment create \
    --role "Storage Blob Data Contributor" \
    --assignee @- \
    --scope "/subscriptions/$AZURE_SUBSCRIPTION_ID/resourceGroups/$AZURE_RESOURCE_GROUP/providers/Microsoft.Storage/storageAccounts/$AZURE_STORAGE_NAME"

    az storage container create \
    --account-name $AZURE_STORAGE_NAME \
    --name  \
    --auth-mode login

    az storage blob directory create \
        --account-name $AZURE_STORAGE_NAME \
        --container-name $AZURE_CONTAINER_NAME \
        --directory-path models \
        --auth-mode login

    echo "Upload a file with:"
    echo 'az storage blob upload \
    --account-name '$AZURE_STORAGE_NAME' \
    --container-name '$AZURE_CONTAINER_NAME' \
    --name myFile.txt \
    --file myFile.txt \
    --auth-mode login'

    echo "Upload a folder with:"
    echo 'az storage blob upload-batch \
    --destination '$AZURE_CONTAINER_NAME'  \
    --account-name '$AZURE_STORAGE_NAME' \
    --destination-path model/1/ \
    --source /path/to/your/folder \
    --auth-mode login'
}

delete_azure_cluster() {
    echo "Deleting ${AZURE_FOLDER}..."
    openshift-install destroy cluster --dir $AZURE_FOLDER --log-level info
}