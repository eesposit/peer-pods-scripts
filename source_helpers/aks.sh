#! /bin/bash

source $(dirname $0)/../configs/aks.conf

install_aks () {
    AZURE_SUBSCRIPTION_ID=$(az account list --query "[?isDefault].id" -o tsv)

    az account set --subscription $AZURE_SUBSCRIPTION_ID

    az group create \
        --name $AZURE_RESOURCE_GROUP \
        --location $AKS_LOCATION

    # az ad sp create-for-rbac \
  	#     --name "caa-${AZURE_RESOURCE_GROUP}"  \
    #     --role "Contributor"   \
    #     --scopes /subscriptions/${AZURE_SUBSCRIPTION_ID}/resourceGroups/${AZURE_RESOURCE_GROUP} \
    #     --query "{ AZURE_CLIENT_ID: appId, AZURE_CLIENT_SECRET: password, AZURE_TENANT_ID: tenant }"

    az sig create \
        --gallery-name "${GALLERY_NAME}" \
        --resource-group "${AZURE_RESOURCE_GROUP}" \
        --location "${AKS_LOCATION}"


    az sig image-definition create \
        --resource-group "${AZURE_RESOURCE_GROUP}" \
        --gallery-name "${GALLERY_NAME}" \
        --gallery-image-definition "${GALLERY_IMAGE_DEF_NAME}" \
        --publisher GreatPublisher \
        --offer GreatOffer \
        --sku GreatSku \
        --os-type "Linux" \
        --os-state "Generalized" \
        --hyper-v-generation "V2" \
        --location "${AKS_LOCATION}" \
        --architecture "x64" \
        --features SecurityType=ConfidentialVmSupported

    mkdir -p qcow2-img && cd qcow2-img

    QCOW2_IMAGE="quay.io/confidential-containers/podvm-generic-ubuntu-amd64:latest"
    curl -LO https://raw.githubusercontent.com/confidential-containers/cloud-api-adaptor/staging/podvm/hack/download-image.sh

    bash download-image.sh $QCOW2_IMAGE . -o podvm.qcow2
    qemu-img convert -O vpc -o subformat=fixed,force_size podvm.qcow2 podvm.vhd

    az storage account create \
    --name $AZURE_STORAGE_ACCOUNT  \
        --resource-group $AZURE_RESOURCE_GROUP \
        --location $AKS_LOCATION \
        --sku Standard_ZRS \
        --encryption-services blob

    az storage container create \
        --account-name $AZURE_STORAGE_ACCOUNT \
        --name $AZURE_STORAGE_CONTAINER \
        --auth-mode login

    AZURE_STORAGE_KEY=$(az storage account keys list --resource-group $AZURE_RESOURCE_GROUP --account-name $AZURE_STORAGE_ACCOUNT --query "[?keyName=='key1'].{Value:value}" --output tsv)

    echo "AZURE_STORAGE_KEY $AZURE_STORAGE_KEY"

    az storage blob upload  --container-name $AZURE_STORAGE_CONTAINER --name podvm.vhd --file podvm.vhd --account-name $AZURE_STORAGE_ACCOUNT

    AZURE_STORAGE_EP=$(az storage account list -g $AZURE_RESOURCE_GROUP --query "[].{uri:primaryEndpoints.blob} | [? contains(uri, '$AZURE_STORAGE_ACCOUNT')]" --output tsv)

    echo "AZURE_STORAGE_EP $AZURE_STORAGE_EP"

    VHD_URI="${AZURE_STORAGE_EP}${AZURE_STORAGE_CONTAINER}/podvm.vhd"

    az sig image-version create \
    --resource-group $AZURE_RESOURCE_GROUP \
    --gallery-name $GALLERY_NAME  \
    --gallery-image-definition $GALLERY_IMAGE_DEF_NAME \
    --gallery-image-version 0.0.1 \
    --target-regions $AKS_LOCATION \
    --os-vhd-uri "$VHD_URI" \
    --os-vhd-storage-account $AZURE_STORAGE_ACCOUNT

   AZURE_IMAGE_ID=$(az sig image-version  list --resource-group  $AZURE_RESOURCE_GROUP --gallery-name $GALLERY_NAME --gallery-image-definition $GALLERY_IMAGE_DEF_NAME --query "[].{Id: id}" --output tsv)

    echo "AZURE_IMAGE_ID $AZURE_IMAGE_ID"
    cd ..
    rm -rf qcow2-img
}

uninstall_aks () {
    echo "After this, you need to run install_aks"
    az group delete --resource-group $AZURE_RESOURCE_GROUP --yes
}

create_aks_cluster () {
    AZURE_SUBSCRIPTION_ID=$(az account list --query "[?isDefault].id" -o tsv)

    az aks create \
        --resource-group "${AZURE_RESOURCE_GROUP}" \
        --node-resource-group "${AKS_RESOURCE_GROUP}" \
        --name "${AKS_CLUSTER_NAME}" \
        --location "${AKS_LOCATION}" \
        --node-count 1 \
        --node-vm-size Standard_F4s_v2 \
        --ssh-key-value "${AKS_KEY}" \
        --admin-username "${AKS_WORKER_USER_NAME}" \
        --os-sku Ubuntu

    az aks get-credentials \
        --resource-group "${AZURE_RESOURCE_GROUP}" \
        --name "${AKS_CLUSTER_NAME}"

    kubectl label nodes --all node.kubernetes.io/worker=

    AZURE_VNET_NAME=$(az network vnet list \
        --resource-group "${AKS_RESOURCE_GROUP}" \
        --query "[0].name" \
        --output tsv)
    echo "AZURE_VNET_NAME $AZURE_VNET_NAME"

    AZURE_SUBNET_ID=$(az network vnet subnet list \
        --resource-group "${AKS_RESOURCE_GROUP}" \
        --vnet-name "${AZURE_VNET_NAME}" \
        --query "[0].id" \
        --output tsv)
    echo "AZURE_SUBNET_ID $AZURE_SUBNET_ID"

    json=$(az ad sp create-for-rbac \
        --name "caa-${AZURE_RESOURCE_GROUP}"  \
        --role "Contributor"   \
        --scopes /subscriptions/${AZURE_SUBSCRIPTION_ID}/resourceGroups/${AZURE_RESOURCE_GROUP} \
        --query "{ AZURE_CLIENT_ID: appId, AZURE_CLIENT_SECRET: password, AZURE_TENANT_ID: tenant }")

    AZURE_CLIENT_ID=$(echo $json |  jq -r .AZURE_CLIENT_ID)
    echo "AZURE_CLIENT_ID $AZURE_CLIENT_ID"

    AZURE_CLIENT_SECRET=$(echo $json |  jq -r .AZURE_CLIENT_SECRET)
    echo "AZURE_CLIENT_SECRET $AZURE_CLIENT_SECRET"

    AZURE_TENANT_ID=$(echo $json |  jq -r .AZURE_TENANT_ID)
    echo "AZURE_TENANT_ID $AZURE_TENANT_ID"

    AZURE_CAA_CLIENT_SECRET=$(az ad sp create-for-rbac \
        -n "caa-${AZURE_RESOURCE_GROUP}" \
        --role Contributor \
        --scopes "/subscriptions/${AZURE_SUBSCRIPTION_ID}/resourceGroups/${AKS_RESOURCE_GROUP}" \
        --query "password")

    AZURE_IMAGE_ID=$(az sig image-version  list --resource-group  $AZURE_RESOURCE_GROUP --gallery-name $GALLERY_NAME --gallery-image-definition $GALLERY_IMAGE_DEF_NAME --query "[].{Id: id}" --output tsv)

    cd $CAA_LOCATION

    cat <<EOF > install/overlays/azure/kustomization.yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
bases:
- ../../yamls
images:
- name: cloud-api-adaptor
  newName: "${CAA_REGISTRY}/cloud-api-adaptor"
  newTag: latest
generatorOptions:
  disableNameSuffixHash: true
configMapGenerator:
- name: peer-pods-cm
  namespace: confidential-containers-system
  literals:
  - CLOUD_PROVIDER="azure"
  - AZURE_SUBSCRIPTION_ID="${AZURE_SUBSCRIPTION_ID}"
  - AZURE_REGION="${AKS_LOCATION}"
  - AZURE_INSTANCE_SIZE="${AKS_INSTANCE_SIZE}"
  - AZURE_RESOURCE_GROUP="${AZURE_RESOURCE_GROUP}"
  - AZURE_SUBNET_ID="${AZURE_SUBNET_ID}"
  - AZURE_IMAGE_ID="${AZURE_IMAGE_ID}"
  - AZURE_TENANT_ID="${AZURE_TENANT_ID}"
  - AZURE_CLIENT_ID="${AZURE_CLIENT_ID}"
secretGenerator:
- name: peer-pods-secret
  namespace: confidential-containers-system
  envs:
  - service-principal.env
- name: ssh-key-secret
  namespace: confidential-containers-system
  files:
  - id_rsa.pub
EOF

    cp $AKS_KEY install/overlays/azure/id_rsa.pub

    cat <<EOF > install/overlays/azure/service-principal.env
AZURE_CLIENT_ID=${AZURE_CLIENT_ID}
AZURE_CLIENT_SECRET=${AZURE_CAA_CLIENT_SECRET}
AZURE_TENANT_ID=${AZURE_TENANT_ID}
EOF
    make
    CLOUD_PROVIDER=azure registry=$CAA_REGISTRY make image
    CLOUD_PROVIDER=azure registry=$CAA_REGISTRY make deploy

    kubectl get pods -n confidential-containers-system
}

delete_aks_cluster () {
    echo "Deleting..."
    az aks delete --resource-group $AZURE_RESOURCE_GROUP --name $AKS_CLUSTER_NAME --yes
    az ad sp delete --id "caa-${AZURE_RESOURCE_GROUP}"
}