#! /bin/bash
set -e

NAMESPACE=$1
FILE=BACKUP_$NAMESPACE.yaml

if [ $# == 0 ]; then
	echo "Usage: $0 namespace"
	exit 1
fi

oc get namespaces/$NAMESPACE

oc get namespace $NAMESPACE -o yaml > $FILE

sed -i '/- kubernetes/d' $FILE

oc proxy &
sleep 1

echo "#############"
curl -k -H "Content-Type: application/yaml" -X PUT --data-binary @$FILE http://127.0.0.1:8001/api/v1/namespaces/$NAMESPACE/finalize
echo "#############"

rm $FILE

kill -9 %%

oc get namespaces/$NAMESPACE