#! /usr/bin/bash

source $(dirname $0)/azure.sh

# echo "sudo is required in order to edit /etc/hosts"
# [ "$UID" -eq 0 ] || exec sudo "$0" "$@"

CL=$1
DNS_RG=$AZURE_RESOURCE_GROUP
SUFFIX=${CL}.${DNS_ZONE}

if [[ -z ${CL} ]]; then
        echo "pass cluster name as argument"
        exit 1
fi


#az network public-ip list --resource-group $RG -o table
#
#

while [ -z $(az network dns record-set list --resource-group ${DNS_RG} -z ${DNS_ZONE} -o tsv --query "[?name == 'api.${CL}' ].CNAMERecord.cname | [0]") ]
do
  echo "waiting for public ip"
  sleep 60
done

CNAME=$(az network dns record-set list --resource-group ${DNS_RG} -z ${DNS_ZONE} -o tsv --query "[?name == 'api.${CL}' ].CNAMERecord.cname | [0]")
PIP=$(dig +short ${CNAME})
echo "ADDING ${PIP} api.${SUFFIX} to /etc/hosts"

echo "#############${CL}##################" | sudo -E tee -a /etc/hosts
echo "${PIP} api.${SUFFIX}"|  sudo -E tee -a /etc/hosts

while [ -z $(az network dns record-set list --resource-group ${DNS_RG} -z ${DNS_ZONE} -o tsv --query  "[?name == '*.apps.${CL}'].ARecords[0].ipv4Address | [0]") ]
do
  echo "waiting for apps ip"
  sleep 60
done

OIP=$(az network dns record-set list --resource-group ${DNS_RG} -z ${DNS_ZONE} -o tsv --query "[?name == '*.apps.${CL}'].ARecords[0].ipv4Address | [0]")

RECORDS=`cat <<EOF
${OIP} console-openshift-console.apps.${SUFFIX}
${OIP} integrated-oauth-server-openshift-authentication.apps.${SUFFIX}
${OIP} oauth-openshift.apps.${SUFFIX}
${OIP} prometheus-k8s-openshift-monitoring.apps.${SUFFIX}
${OIP} grafana-openshift-monitoring.apps.${SUFFIX}
${OIP} default-route-openshift-image-registry.${SUFFIX}
${OIP} hello-openshift-service-default.apps.${SUFFIX}
${OIP} rhods-dashboard-redhat-ods-applications.apps.${SUFFIX}
#################################################
EOF`

echo "ADDING ${RECORDS} to /etc/hosts"
echo "${RECORDS}" | sudo -E tee -a /etc/hosts