#! /bin/bash

create_rhel_image () {
    RHEL_IMAGE_URL=$1

    if [ -z $RHEL_IMAGE_URL ]; then
        echo "Error: provide rhel image path!"
        return
    fi

    # RH organization id and activation key
    ORG_ID=$(cat ~/.rh_subscription/orgid)
    ACTIVATION_KEY=$(cat ~/.rh_subscription/activation_key)
    # alternatively, use RH username and password
    # RH_USERNAME=""
    # RH_PASSWORD=""

    OSC_REPO=~/sandboxed-containers-operator

    if [ -z $RH_USERNAME -o -z $RH_PASSWORD ]; then
        REGISTER_CMD="subscription-manager register --org=${ORG_ID} --activationkey=${ACTIVATION_KEY}"
    else
        REGISTER_CMD="subscription-manager register --username=${RH_USERNAME} --password=${RH_PASSWORD} –auto-attach"
    fi

    virt-customize -a ${RHEL_IMAGE_URL} --run-command "${REGISTER_CMD}"

    cd $OSC_REPO
    git switch dev-preview
    cd podvm
    podman build -t podvm_builder_rhel -f Dockerfile.podvm_builder.rhel

    RHEL_IMAGE_CHECKSUM=$(sha256sum ${RHEL_IMAGE_URL} | awk '{ print $1 }')
    CLOUD_PROVIDER=libvirt

    podman build -t podvm_rhel \
   --build-arg BUILDER_IMG=localhost/podvm_builder_rhel:latest \
   --build-arg CLOUD_PROVIDER=${CLOUD_PROVIDER} \
   --build-arg RHEL_IMAGE_CHECKSUM=${RHEL_IMAGE_CHECKSUM} \
   -v ${RHEL_IMAGE_URL}:/tmp/rhel.qcow2:Z \
   -f Dockerfile.podvm.rhel

   podman save podvm_rhel | tar -xO --no-wildcards-match-slash '*.tar' | tar -x
}

upload_img_to_virsh () {
    IMAGE=$1

    if [ -z $IMAGE ]; then
        echo "Error: provide qcow2 image path!"
        return
    fi

    # podvm-base.qcow2 is hardcoded!
    virsh -c qemu:///system vol-create-as --pool default --name podvm-base.qcow2 --capacity 20G --allocation 2G --prealloc-metadata --format qcow2

    virsh -c qemu:///system vol-upload --vol podvm-base.qcow2 $IMAGE --pool default --sparse
}

rename_podvm_base () {
    NEW_PODVM_NAME=$1

    if [ -z $NEW_PODVM_NAME ]; then
        echo "Error: provide new podvm name!"
        return
    fi

    virsh  -c qemu:///system vol-clone podvm-base.qcow2 $NEW_PODVM_NAME --pool default
    virsh  -c qemu:///system vol-delete podvm-base.qcow2 --pool default
}

get_img_from_quay () {
    QUAY_REPO=$1

    if [ -z $QUAY_REPO ]; then
        echo "Error: provide quay.io repo!"
        return
    fi

    curl https://raw.githubusercontent.com/confidential-containers/cloud-api-adaptor/v0.5.0/podvm/hack/download-image.sh | bash -s $QUAY_REPO . -o podvm-amd64.qcow2'*.tar'
}