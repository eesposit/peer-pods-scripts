#! /bin/bash

# Names of various configs. Optional to modify
# peer pods secret
PP_SECRET="peer-pods-secret"
# configmap
CM="peer-pods-cm"
# ssh key secret
SSH_SECRET="ssh-key-secret"

OP_NAME=openshift-sandboxed-containers-operator
TR_NAME=trustee-operator-system
T_NAME=trustee-operator
T_OPERATOR_VERSION=${T_OPERATOR_VERSION:-"0.1.0"}
TRUSTEE_CATALOG_IMAGE=quay.io/redhat-user-workloads/ose-osc-tenant/trustee-fbc/trustee-fbc-4-16@sha256:2f422e242e5f05d256fd3f3ef0a8525d5be129040af09933df86b1730e9af6db

oc_command () {
    # if [ -z $KUBEC ]; then
        oc $@
    # else
    #     KUBECONFIG=$KUBEC oc $@
    # fi
}

source_all_providers () {
    for file in providers/*.sh; do
      source $file
    done
    for file in providers/microsoft/*.sh; do
        source $file
    done
}

take_provider_from_input () {
    if [ -z $cld ]; then
        PS3="select the cloud-provider: "
        select cld in aws aro azure libvirt
        do
            break
        done
    fi
}

check_providers () {
    case $cld in
        "aws")
            ;;
        "aro")
            ;;
        "azure")
            ;;
        "libvirt")
            ;;
        *)
            echo "Error: provider \"$cld\" not supported."
            exit 1;;
    esac
}

compile_operator () {
    cd $OPERATOR_LOCATION
    make docker-build
    make docker-push
    make bundle CHANNELS=candidate
    make bundle-build
    make bundle-push
    make catalog-build
    make catalog-push
    cd -
}

wait_for_oc_command () {
    set +e
    comm=$@
    while : ; do
        eval oc_command $comm
        if [ $? = 0 ]; then
            echo "Completed!"
            break
        fi
        echo "Retrying..."
        sleep 5
    done
}

ICSP_NAME=kata-brew-registry
ImageContentSourcePolicy="\
apiVersion: operator.openshift.io/v1alpha1
kind: ImageContentSourcePolicy
metadata:
  name: ${ICSP_NAME}
spec:
  repositoryDigestMirrors:
  - mirrors:
    - brew.registry.redhat.io
    source: registry.redhat.io
  - mirrors:
    - brew.registry.redhat.io
    source: registry.stage.redhat.io
  - mirrors:
    - brew.registry.redhat.io
    source: registry-proxy.engineering.redhat.com"

CatalogSource="\
apiVersion: operators.coreos.com/v1alpha1
kind: CatalogSource
metadata:
  name:  ${CATALOG_NAME}
  namespace: openshift-marketplace
spec:
  DisplayName: My Operator Catalog
  sourceType: grpc
  image: ${CATALOG_TAG}"

Namespace="\
apiVersion: v1
kind: Namespace
metadata:
  name: ${OP_NAME}"

OperatorGroup="\
apiVersion: operators.coreos.com/v1
kind: OperatorGroup
metadata:
  name: ${OP_NAME}
  namespace: ${OP_NAME}
spec:
  targetNamespaces:
  - ${OP_NAME}"

S_NAME=sandboxed-containers-operator
Subscription="\
apiVersion: operators.coreos.com/v1alpha1
kind: Subscription
metadata:
  name: ${OP_NAME}
  namespace: ${OP_NAME}
spec:
  channel: stable
  installPlanApproval: Automatic
  name: ${S_NAME}
  source: redhat-operators
  sourceNamespace: openshift-marketplace
  startingCSV: ${S_NAME}.v${OPERATOR_VERSION}"

FeatureGate="\
apiVersion: v1
kind: ConfigMap
metadata:
  name: osc-feature-gates
  namespace: ${OP_NAME}
data:
  confidential: 'true'"

KC_NAME=example-kataconfig
KataConfig="\
apiVersion: kataconfiguration.openshift.io/v1
kind: KataConfig
metadata:
  name: ${KC_NAME}
spec:
  enablePeerPods: true"

HO_NAME=hello-openshift
HelloOpenshift="\
apiVersion: v1
kind: Pod
metadata:
  name: ${HO_NAME}
  namespace: default
  labels:
    app: ${HO_NAME}
spec:
  runtimeClassName: kata-remote
  containers:
    - name: ${HO_NAME}
      image: quay.io/openshift/origin-hello-openshift
      ports:
        - containerPort: 8888
      securityContext:
        privileged: false
        allowPrivilegeEscalation: false
        runAsNonRoot: true
        runAsUser: 1001
        capabilities:
          drop:
            - ALL
        seccompProfile:
          type: RuntimeDefault
---
kind: Service
apiVersion: v1
metadata:
  name: ${HO_NAME}-service
  namespace: default
  labels:
    app: ${HO_NAME}
spec:
  selector:
    app: ${HO_NAME}
  ports:
    - port: 8888"


HD_NAME=hello-debug
HelloDebug="\
apiVersion: v1
kind: Pod
metadata:
  name: ${HD_NAME}
  namespace: default
  labels:
    app: ${HD_NAME}
  annotations:
    io.katacontainers.config.agent.policy: cGFja2FnZSBhZ2VudF9wb2xpY3kKCmRlZmF1bHQgQWRkQVJQTmVpZ2hib3JzUmVxdWVzdCA6PSB0cnVlCmRlZmF1bHQgQWRkU3dhcFJlcXVlc3QgOj0gdHJ1ZQpkZWZhdWx0IENsb3NlU3RkaW5SZXF1ZXN0IDo9IHRydWUKZGVmYXVsdCBDb3B5RmlsZVJlcXVlc3QgOj0gdHJ1ZQpkZWZhdWx0IENyZWF0ZUNvbnRhaW5lclJlcXVlc3QgOj0gdHJ1ZQpkZWZhdWx0IENyZWF0ZVNhbmRib3hSZXF1ZXN0IDo9IHRydWUKZGVmYXVsdCBEZXN0cm95U2FuZGJveFJlcXVlc3QgOj0gdHJ1ZQpkZWZhdWx0IEV4ZWNQcm9jZXNzUmVxdWVzdCA6PSB0cnVlCmRlZmF1bHQgR2V0TWV0cmljc1JlcXVlc3QgOj0gdHJ1ZQpkZWZhdWx0IEdldE9PTUV2ZW50UmVxdWVzdCA6PSB0cnVlCmRlZmF1bHQgR3Vlc3REZXRhaWxzUmVxdWVzdCA6PSB0cnVlCmRlZmF1bHQgTGlzdEludGVyZmFjZXNSZXF1ZXN0IDo9IHRydWUKZGVmYXVsdCBMaXN0Um91dGVzUmVxdWVzdCA6PSB0cnVlCmRlZmF1bHQgTWVtSG90cGx1Z0J5UHJvYmVSZXF1ZXN0IDo9IHRydWUKZGVmYXVsdCBPbmxpbmVDUFVNZW1SZXF1ZXN0IDo9IHRydWUKZGVmYXVsdCBQYXVzZUNvbnRhaW5lclJlcXVlc3QgOj0gdHJ1ZQpkZWZhdWx0IFB1bGxJbWFnZVJlcXVlc3QgOj0gdHJ1ZQpkZWZhdWx0IFJlYWRTdHJlYW1SZXF1ZXN0IDo9IHRydWUKZGVmYXVsdCBSZW1vdmVDb250YWluZXJSZXF1ZXN0IDo9IHRydWUKZGVmYXVsdCBSZW1vdmVTdGFsZVZpcnRpb2ZzU2hhcmVNb3VudHNSZXF1ZXN0IDo9IHRydWUKZGVmYXVsdCBSZXNlZWRSYW5kb21EZXZSZXF1ZXN0IDo9IHRydWUKZGVmYXVsdCBSZXN1bWVDb250YWluZXJSZXF1ZXN0IDo9IHRydWUKZGVmYXVsdCBTZXRHdWVzdERhdGVUaW1lUmVxdWVzdCA6PSB0cnVlCmRlZmF1bHQgU2V0UG9saWN5UmVxdWVzdCA6PSB0cnVlCmRlZmF1bHQgU2lnbmFsUHJvY2Vzc1JlcXVlc3QgOj0gdHJ1ZQpkZWZhdWx0IFN0YXJ0Q29udGFpbmVyUmVxdWVzdCA6PSB0cnVlCmRlZmF1bHQgU3RhcnRUcmFjaW5nUmVxdWVzdCA6PSB0cnVlCmRlZmF1bHQgU3RhdHNDb250YWluZXJSZXF1ZXN0IDo9IHRydWUKZGVmYXVsdCBTdG9wVHJhY2luZ1JlcXVlc3QgOj0gdHJ1ZQpkZWZhdWx0IFR0eVdpblJlc2l6ZVJlcXVlc3QgOj0gdHJ1ZQpkZWZhdWx0IFVwZGF0ZUNvbnRhaW5lclJlcXVlc3QgOj0gdHJ1ZQpkZWZhdWx0IFVwZGF0ZUVwaGVtZXJhbE1vdW50c1JlcXVlc3QgOj0gdHJ1ZQpkZWZhdWx0IFVwZGF0ZUludGVyZmFjZVJlcXVlc3QgOj0gdHJ1ZQpkZWZhdWx0IFVwZGF0ZVJvdXRlc1JlcXVlc3QgOj0gdHJ1ZQpkZWZhdWx0IFdhaXRQcm9jZXNzUmVxdWVzdCA6PSB0cnVlCmRlZmF1bHQgV3JpdGVTdHJlYW1SZXF1ZXN0IDo9IHRydWUK
spec:
  runtimeClassName: kata-remote
  containers:
    - name: ${HD_NAME}
      image: quay.io/openshift/origin-hello-openshift
      ports:
        - containerPort: 8888
      securityContext:
        privileged: false
        allowPrivilegeEscalation: false
        runAsNonRoot: true
        runAsUser: 1001
        capabilities:
          drop:
            - ALL
        seccompProfile:
          type: RuntimeDefault
---
kind: Service
apiVersion: v1
metadata:
  name: ${HD_NAME}-service
  namespace: default
  labels:
    app: ${HD_NAME}
spec:
  selector:
    app: ${HD_NAME}
  ports:
    - port: 8888"


TrusteeCatalogSource="\
apiVersion: operators.coreos.com/v1alpha1
kind: CatalogSource
metadata:
 name:  ${T_NAME}-catalog
 namespace: openshift-marketplace
spec:
 displayName: Trustee Operator Catalog
 sourceType: grpc
 image: ${TRUSTEE_CATALOG_IMAGE}
 updateStrategy:
   registryPoll:
      interval: 5m"

TrusteeNamespace="\
apiVersion: v1
kind: Namespace
metadata:
  name: ${TR_NAME}"

TrusteeOperatorGroup="\
apiVersion: operators.coreos.com/v1
kind: OperatorGroup
metadata:
  name: ${T_NAME}-group
  namespace: ${TR_NAME}
spec:
  targetNamespaces:
  - ${TR_NAME}"

#source: redhat-operators
TrusteeSubscription="\
apiVersion: operators.coreos.com/v1alpha1
kind: Subscription
metadata:
  name: ${T_NAME}
  namespace: ${TR_NAME}
spec:
  channel: stable
  installPlanApproval: Automatic
  name: ${T_NAME}
  source: redhat-operators
  sourceNamespace: openshift-marketplace
  startingCSV: ${T_NAME}.v${T_OPERATOR_VERSION}"

TrusteeCM="\
apiVersion: v1
kind: ConfigMap
metadata:
  name: kbs-config-cm
  namespace: ${TR_NAME}
data:
  kbs-config.json: |
    {
      \"insecure_http\" : true,
      \"sockets\": [\"0.0.0.0:8080\"],
      \"auth_public_key\": \"/etc/auth-secret/publicKey\",
      \"attestation_token_config\": {
        \"attestation_token_type\": \"CoCo\"
      },
      \"repository_config\": {
        \"type\": \"LocalFs\",
        \"dir_path\": \"/opt/confidential-containers/kbs/repository\"
      },
      \"as_config\": {
        \"work_dir\": \"/opt/confidential-containers/attestation-service\",
        \"policy_engine\": \"opa\",
        \"attestation_token_broker\": \"Simple\",
          \"attestation_token_config\": {
          \"duration_min\": 5
          },
        \"rvps_config\": {
          \"store_type\": \"LocalJson\",
          \"store_config\": {
            \"file_path\": \"/opt/confidential-containers/rvps/reference-values/reference-values.json\"
          }
         }
      },
      \"policy_engine_config\": {
        \"policy_path\": \"/opt/confidential-containers/opa/policy.rego\"
      }
    }"

TrusteeRV="\
apiVersion: v1
kind: ConfigMap
metadata:
  name: rvps-reference-values
  namespace: ${TR_NAME}
data:
  reference-values.json: |
    [
    ]"

TrusteeRP="\
apiVersion: v1
kind: ConfigMap
metadata:
  name: resource-policy
  namespace: ${TR_NAME}
data:
  policy.rego: |
    package policy
    default allow = false
    allow {
      input[\"tee\"] != \"sample\"
    }"

KBSConfig="\
apiVersion: confidentialcontainers.org/v1alpha1
kind: KbsConfig
metadata:
  labels:
    app.kubernetes.io/name: kbsconfig
    app.kubernetes.io/instance: kbsconfig
    app.kubernetes.io/part-of: ${T_NAME}
    app.kubernetes.io/managed-by: kustomize
    app.kubernetes.io/created-by: ${T_NAME}
  name: kbsconfig
  namespace: ${TR_NAME}
spec:
  kbsConfigMapName: kbs-config-cm
  kbsAuthSecretName: kbs-auth-public-key
  kbsDeploymentType: AllInOneDeployment
  kbsRvpsRefValuesConfigMapName: rvps-reference-values
  kbsSecretResources: [kbsres1]
  kbsResourcePolicyConfigMapName: resource-policy"