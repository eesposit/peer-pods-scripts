#! /bin/bash
set -e

source common_yaml.sh

temp_configmap=configmap.yaml

oc_command get configmap $CM -n $OP_NAME > /dev/null 2>&1
if [ $? -ne 0 ]; then
    echo "ConfigMap $CM does not exist in namespace $OP_NAME. First run ./install_peerpods.sh"
    exit 1
fi

oc_command get route -n $TR_NAME kbs-service > /dev/null 2>&1
if [ $? -ne 0 ]; then
    echo "Route kbs-service does not exist in namespace $TR_NAME. First run ./install_trustee.sh"
    exit 1
fi

echo "#### Getting old configmap..."
oc get configmap $CM -n $OP_NAME -o yaml > $temp_configmap
cat $temp_configmap
echo

echo "#### Getting route..."
TRUSTEE_HOST=$(oc_command get route -n $TR_NAME kbs-service -o jsonpath={.spec.host})
echo $TRUSTEE_HOST
echo

echo "#### Updating configmap..."
AA_KBC_PARAMS="AA_KBC_PARAMS: \"cc_kbc::https://${TRUSTEE_HOST}\""
sed -i "/^data:/a\  $AA_KBC_PARAMS" $temp_configmap
oc_command apply -f $temp_configmap
echo

oc get configmap $CM -n $OP_NAME -o yaml
echo

oc set env ds/peerpodconfig-ctrl-caa-daemon -n $OP_NAME REBOOT="$(date)"

rm $temp_configmap

echo "#### Testing if it works..."
oc_command apply -f-<<EOF
${HelloDebug}
EOF

echo "Waiting that hello debug pod is running... (might take a while)"
cmd="wait --for=condition=Ready pod/hello-debug --timeout=-1s"
wait_for_oc_command $cmd
oc_command get pod/hello-debug
echo
oc_command exec $HD_NAME -- curl -s http://127.0.0.1:8006/cdh/resource/default/kbsres1/key1
echo